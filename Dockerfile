FROM nginx:mainline-alpine
EXPOSE 80
WORKDIR /code
RUN apk add nginx zola
ADD . .
RUN zola build
ADD nginx.conf /etc/nginx/conf.d/default.conf