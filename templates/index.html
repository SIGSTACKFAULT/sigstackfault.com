{% extends "base.html" %}

{% macro project(name, desc="", url="", width=1) %}
<li class="project col-span-{{width}}">
    <h3> <a href="{{ url}}"> {{ name | safe }} </a> </h3>
    <p> {{ desc | markdown | safe }} </p>
</li>
{% endmacro project %}

{% block header %}
<div class="flex">
    <div class="header__header">
        <h1>
            Hi, I'm <s class="old-username" style="display: none; text-decoration-thickness: .15em;">Blacksilver</s>
            <b>SIGSTACKFAULT!</b>
        </h1>
        <p class="old-username" style="display: none; font-style: italic; font-size: 1rem; margin: 0">
            It's my username and I can change it if I want!
        </p>
    </div>
    <div class="header__rad">
        <img src="/rad.svg" alt="Radiation symbol (my profile pic)">
    </div>
</div>
<script>
    // handle requests redirected from blacksilver.xyz
    var searchParams = new URLSearchParams(window.location.search);
    if (searchParams.get("blacksilver") === "1") {
        document.querySelectorAll(".old-username").forEach(e => e.style.display = null);
    }

    // make icon spin
    var header = document.getElementsByTagName("header")[0]
    var rad = document.querySelector(".header__rad img")
    function spin_mouseenterhandler() {
        //console.log("starting");
        rad.removeEventListener("animationiteration", spin_iterationeventhandler);
        rad.classList.add("spin")
    }
    function spin_mouseleavehandler() {
        rad.addEventListener("animationiteration", spin_iterationeventhandler)
    }
    function spin_iterationeventhandler() {
        //console.log("stopping");
        rad.classList.remove("spin")
        rad.removeEventListener("animationiteration", spin_iterationeventhandler);
    }
    header.addEventListener("mouseenter", spin_mouseenterhandler)
    header.addEventListener("mouseleave", spin_mouseleavehandler)
    spin_mouseenterhandler()
    spin_mouseleavehandler()
</script>
<style>
    @keyframes spin {
        0% {
            transform: rotate(0);
            animation-timing-function: cubic-bezier(0.645, 0.045, 0.355, 1);
        }

        90% {
            transform: rotate(480deg);
        }

        100% {
            transform: rotate(480deg);
        }
    }

    .spin {
        animation: spin 2.5s infinite;
        transform-origin: 50% 50%;
    }
</style>
{% endblock %}

{% block main %}
<section>
    <h2>About me</h2>
    <p>I'm not picky; I'm perfectly happy coding frontend or backend. I like writing tools to help other people do their
        jobs, like test automation.</p>
    <p><b>Fluent in:</b> C &bull; Python &bull; Rust &bull; HTML &bull; CSS &bull; JS &bull; TS &bull; Vim</p>
    <p>
        <s>I daily drive <a href="https://nixos.org/">NixOS</a> but I won't claim to be very good at it.</s>
        I switched to PopOS out of anger at some games not working.
        I think I'll try EndeavourOS next.
    </p>
    <p>
        <code style="font-size: 1.25em;" id="email">sigstackfault@gmail.com</code>
        <button onclick="navigator.clipboard.writeText(document.getElementById('email').innerText)">copy</button>
    </p>
</section>
<section>
    <h2>Projects</h2>
    <ul class="projects">
        {{ self::project(
        name="Bevy-Website: The One True Search&trade;",
        url="https://github.com/bevyengine/bevy-website/pull/1204",
        desc="The ultimate search dialog for [Bevy's website](https://bevyengine.org).",
        )}}
        {{ self::project(
        name="sigstackfault.com",
        url="https://gitlab.com/SIGSTACKFAULT/sigstackfault.com",
        desc="This website! Made with Zola.",
        )}}
        {{ self::project(
        name="arghelper",
        url="https://gitlab.com/SIGSTACKFAULT/arghelper",
        desc="Discord bot in Python to help with solving Alternate Reality Games",
        )}}
        {{ self::project(
        name="ROBOT9000",
        url="https://gitlab.com/SIGSTACKFAULT/robot9000",
        desc="Discord implementation of a very impractial moderation bot invented by xkcd. Originally in Python, now in
        Rust!",
        width=2,
        )}}
        {{ self::project(
        name="Fleetwright Web Designer",
        url="https://gitlab.com/SIGSTACKFAULT/fleetwright-web-designer",
        desc="Editor & Virtual Tabletop for a wargame a friend invented. Live website
        [here](https://sigstackfault.gitlab.io/fleetwright-web-designer)!",
        width=2,
        )}}
        {{ self::project(
        name="SbTeX",
        url="https://gitlab.com/SbTeX/SbTeX",
        desc="Python helper tool specific to the same ARG as ROBOT9000. No actual relation to TeX.",
        )}}
    </ul>
</section>
{% endblock main %}